const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (request, response) => {
  const {
    emp_Name,
    emp_Address,
    emp_Email,
    emp_MobileNo,
    emp_DOB,
    emp_JoiningDate,
  } = request.body

  const statement = `insert into employee(emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate) values (?,?,?,?,?,?)`

  db.pool.query(
    statement,
    [emp_Name, emp_Address, emp_Email, emp_MobileNo, emp_DOB, emp_JoiningDate],
    (error, data) => {
      response.send(utils.createResult(error, data))
    }
  )
})

router.get('/', (request, response) => {
  console.log('inside get employee')
  const statement = `select emp_id, emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate from employee`

  db.pool.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.delete('/:emp_id', (request, response) => {
  const { emp_id } = request.params
  console.log(emp_id)
  const statement = `delete from employee where emp_id =?`

  db.pool.query(statement, [emp_id], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.put('/:emp_id', (request, response) => {
  const {
    emp_Name,
    emp_Address,
    emp_Email,
    emp_MobileNo,
    emp_DOB,
    emp_JoiningDate,
  } = request.body

  const { emp_id } = request.params

  const statement = `update employee set emp_Name =? ,emp_Address=? ,emp_Email=?,emp_MobileNo=?,emp_DOB=?,emp_JoiningDate=? where emp_id =?`

  db.pool.query(
    statement,
    [
      emp_Name,
      emp_Address,
      emp_Email,
      emp_MobileNo,
      emp_DOB,
      emp_JoiningDate,
      emp_id,
    ],
    (error, data) => {
      response.send(utils.createResult(error, data))
    }
  )
})

module.exports = router

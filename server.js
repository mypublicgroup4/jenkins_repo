const express = require('express')

const employeeRouter = require('./routes/employee')

const app = express()

app.use(express.json())

app.use('/employee', employeeRouter)

app.listen(5000, '0.0.0.0', () => {
  console.log('server started on port 5000')
})

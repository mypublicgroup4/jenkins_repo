const mysql = require('mysql')

const pool = mysql.createPool({
  user: 'root',
  host: 'localhost',
  database: 'lab_exam_asdm',
  password: 'manager',
  connectionLimit: 20,
  port: 3306,
})

module.exports = { pool }
